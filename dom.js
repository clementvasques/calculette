// -------------- Calculette -----------------------------

// TODO 

//  Bugs sur la virgule 
//  Multiplicateur : 9*9****
//  Diviser par 0 : infinity
//  Si finit par bouton spécial erreur


// ----- SELECTEURS
let affichageEcran = document.querySelector(".centreEcran")
let allDigit = document.querySelectorAll('.digit')
let allSpecificButton = document.querySelectorAll(".speciaux")
let resetButton = document.querySelector(".reset")
let egalButton = document.querySelector('.degradeEgal')
let multipleButton = document.querySelector(".multiple")
let plusButton = document.querySelector(".plus")
let corrigeButton = document.querySelector(".corrige")
let moinsButton = document.querySelector(".moins")
let diviseButton = document.querySelector(".divise")
let pointButton = document.querySelector(".point")
let zeroButton = document.querySelector(".boutonZero")


// ----- COMPTEURS
let compteurPlus = 0
let compteurMoins = 0
let compteurDivise = 0
let compteurMultiple = 0
let compteurEgal = 0
let compteurPoint = 0
let compteurZero = 0
let bloqueZeroPoint = 0
let compteurPointDeux = 0


corrigeButton.addEventListener('click', function(){
    compteurPoint = 0
    compteurMultiple = 0
    compteurPlus = 0
    compteurMoins = 0
    compteurDivise = 0
    compteurEgal = 0
    bloqueZeroPoint = 1
    compteurPointDeux = 0
    let affichageBoutonCorrige = affichageEcran.textContent
    affichageEcran.textContent = affichageEcran.textContent.substr(0, affichageBoutonCorrige.length-1)
})

resetButton.addEventListener('click', function(){
    affichageEcran.textContent = ""
    compteurMoins = 0
    compteurPlus = 0
    compteurDivise = 0
    compteurMultiple = 0
    compteurEgal = 0
    compteurPoint = 0
    compteurZero = 0
    bloqueZeroPoint = 0
    compteurPointDeux = 0
})

diviseButton.addEventListener('click', function(){
    compteurDivise++
    compteurPoint = 1
    compteurMoins = 1
    compteurPlus = 1
    compteurMultiple = 1
    compteurEgal = 0
    compteurPointDeux = 0

    compteurZero = 0
    bloqueZeroPoint = 0
    if (compteurDivise===1){
        let AffichageBoutonDivise = diviseButton.textContent.trim()
        affichageEcran.textContent = affichageEcran.textContent + AffichageBoutonDivise
    } else {
        let affichage = affichageEcran.textContent
        let AffichageBoutonDivise = diviseButton.textContent.substr(0, affichage.length-1).trim()
        affichageEcran.textContent = affichageEcran.textContent + AffichageBoutonDivise
    }
})

pointButton.addEventListener('click', function(){
    bloqueZeroPoint++
    compteurPoint++

    compteurMoins = 1
    compteurPlus = 1
    compteurDivise = 1
    compteurMultiple = 1

    compteurZero = 0
    compteurEgal = 0
    

    if (compteurPointDeux>1) {
        let affichage = affichageEcran.textContent
        let AffichageBoutonPoint = pointButton.textContent.substr(0, affichage.length-1).trim()
        affichageEcran.textContent = affichageEcran.textContent + AffichageBoutonPoint
    } else if (compteurPoint===1){
        let AffichageBoutonPoint = pointButton.textContent.trim()
        affichageEcran.textContent = affichageEcran.textContent + AffichageBoutonPoint
    } 
    // TODO : 64.9. & 36.*
})

for (let button of allDigit){
    button.addEventListener('click', function(){
        compteurPoint=0 

        compteurPointDeux++

        compteurDivise = 0
        compteurPlus = 0
        compteurMoins = 0
        compteurMultiple = 0

        compteurZero = 0
        bloqueZeroPoint = 1

        console.log(compteurZero)
        if (compteurEgal===1){
            affichageEcran.textContent = ""
            compteurEgal--
        }

        affichageEcran.textContent = affichageEcran.textContent + button.textContent.trim()
    })
}

moinsButton.addEventListener('click', function(){
    compteurMoins++
    compteurPoint = 1
    compteurPlus = 1
    compteurDivise = 1
    compteurMultiple = 1
    compteurEgal = 0
    compteurPointDeux = 0

    compteurZero = 0
    bloqueZeroPoint = 0

    if (compteurMoins===1){
        let AffichageBoutonMoins = moinsButton.textContent.trim()
        affichageEcran.textContent = affichageEcran.textContent + AffichageBoutonMoins
    } else {
        let affichage = affichageEcran.textContent
        let AffichageBoutonMoins = moinsButton.textContent.substr(0, affichage.length-1).trim()
        affichageEcran.textContent = affichageEcran.textContent + AffichageBoutonMoins
    }
})

zeroButton.addEventListener('click', function(){
    compteurZero++
    compteurPointDeux++

    compteurPlus = 0
    compteurPoint = 0
    compteurDivise = 0
    compteurMoins = 0
    compteurMultiple = 0
    compteurEgal = 0

    if (compteurZero===1){  
        let AffichageBoutonZero = zeroButton.textContent.trim()
        affichageEcran.textContent = affichageEcran.textContent + AffichageBoutonZero

        if (bloqueZeroPoint===1){
            // Si 0. => 0.0000
            compteurZero--
        } 
    } else {  
        let affichage = affichageEcran.textContent
        let AffichageBoutonZero = zeroButton.textContent.substr(0, affichage.length-1).trim()
        affichageEcran.textContent = affichageEcran.textContent + AffichageBoutonZero
        console.log("compteur point", compteurPoint)
        console.log("compteur Zero", compteurZero)
        console.log("else")
    }
})

plusButton.addEventListener('click', function(){
    compteurPlus++
    compteurPoint = 1
    compteurDivise = 1
    compteurMoins = 1
    compteurMultiple = 1
    compteurEgal = 0

    compteurPointDeux = 0
    console.log("compteurPointDeux", compteurPointDeux)

    bloqueZeroPoint = 0
    if (compteurPlus===1){
        let AffichageBoutonPlus = plusButton.textContent.trim()
        affichageEcran.textContent = affichageEcran.textContent + AffichageBoutonPlus
    } else {
        let affichage = affichageEcran.textContent
        let AffichageBoutonPlus = plusButton.textContent.substr(0, affichage.length-1).trim()
        affichageEcran.textContent = affichageEcran.textContent + AffichageBoutonPlus
    }
})


multipleButton.addEventListener('click', function(){
    compteurMultiple++
    compteurPoint = 1
    compteurDivise = 1
    compteurMoins = 1
    compteurPlus = 1
    compteurEgal = 0
    bloqueZeroPoint = 0
    compteurPointDeux = 0

    if (compteurMultiple===1){
        let AffichageBoutonMultiple = multipleButton.textContent.replaceAll("x", "*").trim()
        affichageEcran.textContent = affichageEcran.textContent + AffichageBoutonMultiple
    } else {
        let affichage = affichageEcran.textContent
        let AffichageBoutonMultiple = multipleButton.textContent.replace(/x/i, "*").trim().substr(0, affichage.length-2) 
        affichageEcran.textContent = affichageEcran.textContent + AffichageBoutonMultiple
    }    
    // TODO : DEBUG length-2 ? ; 6*5***
})

egalButton.addEventListener('click', function(){
    try{
        compteurEgal++
        let calcul = affichageEcran.textContent.trim() 
        let result = eval(calcul)

        affichageEcran.textContent = result
    } catch (error){
        affichageEcran.textContent = "Erreur"
    }
})





